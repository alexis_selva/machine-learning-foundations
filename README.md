These are the programming assignments relative to Machine Learning - Foundatations (1st part):

* Assignment 1: getting Started with Python & iPython Notebooks
* Assignment 2: predicting house prices
* Assignment 3: analyzing product sentiment
* Assignment 4: retrieving document
* Assignment 5: recommending song
* Assignment 6: classifying/retrieving image using deep features

For more information, I invite you to have a look at https://www.coursera.org/learn/ml-foundations